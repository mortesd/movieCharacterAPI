package com.assignment5.movieCharacterAPI.controllers;

import com.assignment5.movieCharacterAPI.models.Franchise;
import com.assignment5.movieCharacterAPI.repositories.MovieRepository;
import com.assignment5.movieCharacterAPI.models.Character;
import com.assignment5.movieCharacterAPI.models.Movie;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping(value = "api/v1/movies")
public class MovieController {
    @Autowired
    private MovieRepository movieRepository;

    @Operation(summary = "Get all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the movies",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
    })
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> movies = movieRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(movies, resp);
    }

    @Operation(summary = "Get a movie by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })

    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable Long id) {
        Movie returnMovie = new Movie();
        HttpStatus resp;

        if(movieRepository.existsById(id)) {
            System.out.println("Found movie with id: " + id);
            returnMovie = movieRepository.findById(id).get();
            resp = HttpStatus.OK;
        } else {
            System.out.println("Movie with id " + id + " not found");
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovie, resp);
    }

    @Operation(summary = "Get all characters in a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the characters in the movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })

    @GetMapping("/{id}/characters")
    public ResponseEntity<Set<Character>> getCharactersInMovie(@PathVariable Long id) {
        Set<Character> characters = null;
        HttpStatus resp;
        if(movieRepository.existsById(id)) {
            System.out.println("Found franchise with id " + id);
            Movie movie = movieRepository.findById(id).get();
            characters = movie.characters;
            resp = HttpStatus.OK;
        }else {
            System.out.println("Could not find movie with id " + id);
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(characters, resp);
    }

    @Operation(summary = "Add a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Added a movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
    })
    @PostMapping
    public ResponseEntity<Movie> addNewMovie(@RequestBody Movie movie) {
        Movie returnMovie = movieRepository.save(movie);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnMovie, status);
    }

    @Operation(summary = "Update a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Updated the movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "400", description = "Id in body does not match path id",
                    content = @Content) })
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movie) {
        Movie returnMovie = new Movie();
        HttpStatus status;

        if(!id.equals(movie.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie, status);
        }

        returnMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnMovie, status);
    }

    @Operation(summary = "Delete a movie by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The movie was deleted",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Movie> deleteMovie(@PathVariable Long id) {
        Movie returnMovie = new Movie();
        HttpStatus status;
        if(movieRepository.existsById(id)){
            status = HttpStatus.OK;
            Movie deleteMovie = movieRepository.findById(id).get();
            // Remove the movie from the list in franchise, if the movie is part of a franchise
            Franchise franchise = deleteMovie.franchise;
            if(franchise != null) deleteMovie.franchise.getMovies().remove(deleteMovie);
            // Remove the movie from the movie list in each character in this movie
            Set<Character> characters = deleteMovie.characters;
            // Remove it. No need to check if there are any characters, as it simply won't loop if there are none
            for(Character character : characters) character.movies.remove(deleteMovie);
            movieRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovie, status);
    }

}
