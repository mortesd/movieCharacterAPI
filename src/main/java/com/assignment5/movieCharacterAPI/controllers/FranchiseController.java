package com.assignment5.movieCharacterAPI.controllers;


import com.assignment5.movieCharacterAPI.models.Character;

import com.assignment5.movieCharacterAPI.repositories.FranchiseRepository;

import com.assignment5.movieCharacterAPI.models.Franchise;
import com.assignment5.movieCharacterAPI.models.Movie;
import com.assignment5.movieCharacterAPI.repositories.MovieRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import java.util.Set;


@RestController
@RequestMapping(value = "api/v1/franchises")
public class FranchiseController {

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private MovieRepository movieRepository;

    //Returns all franchises in the database.
    @GetMapping
    @Operation(summary = "Get all franchises in the database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "franchises returned",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Character not found",
                    content = @Content)})
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(franchises, resp);
    }

    //Adds a franchise object to the database. Assgins a new Franchise object to the requested body.
    @PostMapping()
    @Operation(summary = "Add a franchise to the database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Franchise added",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "franchise not found",
                    content = @Content)})
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        Franchise returnFranchise = franchiseRepository.save(franchise);
        System.out.println("New franchise: " + franchise.getId());
        HttpStatus resp = HttpStatus.CREATED;
        return new ResponseEntity<>(returnFranchise, resp);
    }

    //Returns a franchise object by Id. Checks if id exists in db and returns it.
    @Operation(summary = "Get a franchise by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the franchise",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Character not found",
                    content = @Content)})
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchiseById(@PathVariable Long id) {
        Franchise returnFranchise = new Franchise();
        HttpStatus resp;

        if (franchiseRepository.existsById(id)) {
            System.out.println("Franchise with id: " + id);
            returnFranchise = franchiseRepository.findById(id).get();
            resp = HttpStatus.OK;
        } else {
            System.out.println("Franchise not found");
            returnFranchise = null;
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity(returnFranchise, resp);
    }

    //updates a requested francies. Takes a pathvariable for id and a requests a franchise object in the body.
    @Operation(summary = "Update a given Franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Franchise updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "franchise not found",
                    content = @Content)})
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;
        //if it doesnt exist or pathvariable doesnt match id from object requested it returns a BAD_REQUEST.
        if (!id.equals(franchise.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise, status);
        }
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnFranchise, status);
    }

    //Deletes a franchise recognized by id from the database.
    @DeleteMapping(value = "/{id}")
    @Operation(summary = "Delete a franchise from the database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Franchise deleted",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "franchise not found",
                    content = @Content)})
    public HttpStatus deleteFranchise(@PathVariable Long id) {
        HttpStatus status;
        //Checks if it exists in db.
        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            // Gets movies connected with the given franchise.
            // For each movie in the franchise, sets their franchise to null to remove connection both ways.
            Set<Movie> moviesInFranchise = franchiseRepository.getOne(id).getMovies();
            for (Movie movie : moviesInFranchise) {
                movie.setFranchise(null);
                movieRepository.getMovieByFranchiseId(id).remove(movie);
            }
            franchiseRepository.deleteById(id);
            System.out.println("Franchise successfully deleted from database");
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }

    //returns the movies connected to a franchise by franchise id.
    @Operation(summary = "Get movies by franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the franchise",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content)})
    @GetMapping("/{id}/movies")
    public ResponseEntity<Set<Movie>> getMoviesByFranchiseId(@PathVariable Long id) {
        Set<Movie> moviesByFranchiseId = null;
        HttpStatus resp;
        if(franchiseRepository.existsById(id)) {
            System.out.println("found franchise by id: " +id);
            resp = HttpStatus.OK;
            moviesByFranchiseId = franchiseRepository.getOne(id).getMovies();
        }else {
            System.out.println("not found");
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(moviesByFranchiseId, resp);
    }

    //returns all character in a franchise. This means all characters that is in a movie which is connected to a franchise.
    @Operation(summary = "Get characters by franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the franchise",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content)})
    @GetMapping("/{id}/characters")
    public ResponseEntity<List<Character>> getCharactersByFranchise(@PathVariable Long id) {
        List<Character> charactersInFranchise = new ArrayList<>();
        HttpStatus resp;

        if(franchiseRepository.existsById(id)) {
            Franchise franchise = franchiseRepository.findById(id).get();
            Set<Movie> moviesInFranchise = franchise.getMovies();

            //iterates throug all movies in the franchise and adds the characters to return list. If a character is in two movies in the same
            //franchise it is not added twice.
            for (Movie movie : moviesInFranchise) {
                for (Character ch : movie.characters) {
                    if(!charactersInFranchise.contains(ch)) {
                        charactersInFranchise.add(ch);
                    }
                }
            }
            resp = HttpStatus.OK;
            System.out.println("Franchise found");

        }else{
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(charactersInFranchise, resp);
    }
}

