package com.assignment5.movieCharacterAPI.controllers;

import com.assignment5.movieCharacterAPI.repositories.CharacterRepository;
import com.assignment5.movieCharacterAPI.models.Character;
import com.assignment5.movieCharacterAPI.models.Movie;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "api/v1/characters")
public class CharacterController {

    @Autowired
    private CharacterRepository characterRepository;

    //Swagger notation
    @Operation(summary = "Get all characters from the database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all characters",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) })})
    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacters() {
        List<Character> characters = characterRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(characters, resp);
    }

    //Swagger notation
    @Operation(summary = "Get a character by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the character",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Character not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public ResponseEntity<Character> getCharacterById(@Parameter(description = "Id of character to be searched") @PathVariable Long id) {
        Character returnCharacter = new Character();
        HttpStatus status;
        if(characterRepository.existsById(id)){  //check if character does exist
            status = HttpStatus.OK;
            returnCharacter = characterRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCharacter, status);
    }

    //Swagger notation
    @Operation(summary = "Update a character by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the character",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @PutMapping("/{id}")
    public ResponseEntity<Character> updateCharacter(@Parameter(description = "Id of character to be updated")  @PathVariable Long id, @io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The updated character", required = true, content = @Content(
            schema = @Schema(implementation = Character.class))) @RequestBody Character character) {
        Character returnCharacter = new Character();
        HttpStatus status;
        if(!id.equals(character.getId())){  // check for manipulated data
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnCharacter,status);
        }
        returnCharacter = characterRepository.save(character);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnCharacter, status);
    }

    //Swagger notation
    @Operation(summary = "Create a character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created a new character",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) })})
    @PostMapping
    public ResponseEntity<Character> addCharacter(@io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The new character", required = true, content = @Content(
                    schema = @Schema(implementation = Character.class))) @RequestBody Character character) {
        Character returnCharacter = characterRepository.save(character);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnCharacter, status);
    }

    //Swagger notation
    @Operation(summary = "Delete a character by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted the character",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content) })
    @DeleteMapping(value = "/{id}")
    public HttpStatus deleteCharacter(@Parameter(description = "Id of character to be deleted") @PathVariable Long id) {
        HttpStatus status;
        if(characterRepository.existsById(id)){  //check if character exist
            status = HttpStatus.OK;
            Character deleteCharacter = characterRepository.findById(id).get();
            // Find all movies containing this character
            Set<Movie> movies = deleteCharacter.movies;
            // Remove the character from these movies
            for(Movie movie : movies) movie.characters.remove(deleteCharacter);
            characterRepository.deleteById(id);  // delete the character
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }

}
