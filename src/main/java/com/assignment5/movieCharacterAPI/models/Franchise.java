package com.assignment5.movieCharacterAPI.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


//Franchise Entity.
@Entity
@Table(name="franchise")
public class Franchise {

    //autoincrements ID when new franchises are created.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="name")
    private String name;

    @Column(name="description")
    private String description;

    //One to many with movies. A franchise can have many movies, but a movie can only have one franchise.
    @OneToMany(mappedBy="franchise")
    Set<Movie> movies;

    @JsonGetter("movies")
    public List<String> moviesGetter() {
        if(movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return "/api/v1/movies/" + movie.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

 public Franchise() {

    }

    public Franchise(Set<Movie> movies, Long id, String name, String description) {
        this.movies = movies;
        this.id = id;
        this.name = name;
        this.description = description;
    }

    //getters and setters
    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
