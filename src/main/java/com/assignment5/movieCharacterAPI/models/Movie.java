package com.assignment5.movieCharacterAPI.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name="movie")
public class Movie {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name="title")
    private String title;

    @Column(name="genre")
    private String genre;

    @Column(name="release_year")
    private short releaseYear;

    @Column(name="director")
    private String director;

    @Column(name="picture")
    private String picture;

    @Column(name="trailer")
    private String trailer;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="franchise_id")
    public Franchise franchise;

    @JsonGetter("franchise")
    public String franchiseGetter() {
        if(franchise != null) {
            return "/api/v1/franchises/" + franchise.getId();
        }
        return null;
    }

    @ManyToMany(mappedBy="movies")
    public Set<Character> characters;

    @JsonGetter("characters")
    public List<String> charactersGetter() {
        if(characters != null) {
            return characters.stream()
                    .map(character -> {
                        return "/api/v1/characters/" + character.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public short getReleaseYear() {
        return releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public String getPicture() {
        return picture;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

}
