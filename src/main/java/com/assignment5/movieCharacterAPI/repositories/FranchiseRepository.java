package com.assignment5.movieCharacterAPI.repositories;

import com.assignment5.movieCharacterAPI.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise,Long> {



}
