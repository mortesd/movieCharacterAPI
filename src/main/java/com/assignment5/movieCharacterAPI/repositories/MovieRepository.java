package com.assignment5.movieCharacterAPI.repositories;

import com.assignment5.movieCharacterAPI.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    Set<Movie> getMovieByFranchiseId(long franchise_id);

}
