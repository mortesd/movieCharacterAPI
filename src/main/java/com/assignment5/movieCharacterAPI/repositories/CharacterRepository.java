package com.assignment5.movieCharacterAPI.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.assignment5.movieCharacterAPI.models.Character;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {

}
