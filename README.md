# Assignment 5 - MovieCharacterAPI

An API for characters, movies and franchises. The API was created by creating a Spring Boot project,
using Spring Data for data access and Hibernate for object-relational mapping (ORM). 

## Group members

- Morten Svedjan Dahl
- Håkon Dalen Hestnes
- Michael Mellemseter
- Sondre Relling

## How to run

You can visit https://movie-characters-api.herokuapp.com/ to access the API, or you can run it locally by cloning the project and run it in an IDE. If you want to run it locally you have to create your own application.properties file in src/main/resources with the right logins for your PostgreSQL database.

## Database

This project is using a PostgreSQL database that is published on Heroku with the application itself.

## Postman

This project contains a set API calls from Postman, located in the test folder, that can be used to test the different endpoints.

## Documentation

To access the documentation, visit https://movie-characters-api.herokuapp.com/swagger-ui.html

## Structure

The application has the following structure. 
* controllers 
    * CharacterController
    * FranchiseController
    * MovieController
* models
    * Character
    * Franchise
    * Movie
* repositories
    * CharacterRepository
    * FranchiseRepository
    * MovieRepository
* resources
    * application.properties
* MovieCharacterApiApplication (main program)

#### Explanation.
The controllers holds the endpoints of every method. For each controller, there is one connecting model and one connecting repository. 
The models are classes which holds data about them inside the application, and they create the tables of the database. They initalize the connection between the models, for example they set the connection between Franchise to Movie with OneToMany and ManyToOne respectivly. These are then created in the database with the columns based on fields of the model-class. 
The repositories extends the JpaRepository and allows the controllers to use functionality from these repositories. By doing this the controllers can retrieve data in the endpoints without having standard SQL queries, and be solely reliant on methods from the JpaRepository. For example can one by calling FranchiseRepository.findAll() retrieve all the franchises in the database, where findAll() is a method from the JpaRepository. 
Every endpoint is annotated with "@ Operation", allowing documentation through swagger. 
The recources folder holds the application.properties file which handles the connection to the PostgreSQL database. 
